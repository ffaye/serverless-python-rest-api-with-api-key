<!--
title: 'AWS Simple REST Endpoint example in Python'
description: 'This template demonstrates how to make a simple REST API with Python running on AWS Lambda and API Gateway using the Serverless Framework.'
layout: Doc
framework: v3
platform: AWS
language: python
-->

# Serverless Framework Python REST API on AWS

This template demonstrates how to make a simple REST API with Python running on AWS Lambda and API Gateway using the Serverless Framework. For further details, see [the official documentation](https://www.serverless.com/framework/docs/providers/aws/events/apigateway/).

This template does not include any kind of persistence (database). For more advanced examples, check out the [serverless/examples repository](https://github.com/serverless/examples/)  which includes DynamoDB, Mongo, Fauna and other examples.

## Setup

1. Install `nodejs`, `npm` and `serverless` (`npm install -g serverless`)
1. Configure AWS credentials: https://www.serverless.com/framework/docs/providers/aws/guide/credentials

## Usage

### Deployment

```
serverless deploy
```

After deploying, you should see output similar to:

```bash
Deploying my-api to stage dev (us-east-1)

✔ Service deployed to stack my-api-dev (114s)

api keys:
  dev-my-api-key: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
endpoints:
  GET - https://xxxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/hello
  GET - https://xxxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/goodbye
functions:
  hello: my-api-dev-hello (2 kB)
  goodbye: my-api-dev-goodbye (2 kB)
```

The above deploys to the `dev` stage by default. Deploy to another stage using the `stage` flag, e.g.

```bash
sls deploy --stage prod
```

### Invocation

After successful deployment, you can call the created application via HTTP:

```bash
curl https://xxxxxxx.execute-api.us-east-1.amazonaws.com/dev/hello -H "x-api-key: $MY_API_KEY"
```

where `$MY_API_KEY` is the API key from the deployment output above exported to an environment variable.

The above should result in response similar to the following (removed `input` content for brevity):

```json
{
  "message": "Hello, World!",
  "input": {
    ...
  }
}
```

### Local development

You can invoke your function locally by using the following command:

```bash
serverless invoke local --function hello
```

Which should result in response similar to the following:

```
{
    "statusCode": 200,
    "body": "{\"message\": \"Hello, World!\", \"input\": {}}"
}

```

Alternatively, it is also possible to emulate API Gateway and Lambda locally by using `serverless-offline` plugin. In order to do that, execute the following command:

```bash
serverless plugin install -n serverless-offline
```

It will add the `serverless-offline` plugin to `devDependencies` in `package.json` file as well as will add it to `plugins` in `serverless.yml`.

After installation, you can start local emulation with:

```
serverless offline
```

To learn more about the capabilities of `serverless-offline`, please refer to its [GitHub repository](https://github.com/dherault/serverless-offline).

### Bundling dependencies

In case you would like to include 3rd party dependencies, you will need to use a plugin called `serverless-python-requirements`. You can set it up by running the following command:

```bash
serverless plugin install -n serverless-python-requirements
```

Running the above will automatically add `serverless-python-requirements` to `plugins` section in your `serverless.yml` file and add it as a `devDependency` to `package.json` file. The `package.json` file will be automatically created if it doesn't exist beforehand. Now you will be able to add your dependencies to `requirements.txt` file and they will be automatically injected to Lambda package during build process.

Note that your local Python version will have to align with the one specified in the `serverless.yml` file for deployment to succeed.
For more details about the plugin's configuration, please refer to [official documentation](https://github.com/UnitedIncome/serverless-python-requirements).
